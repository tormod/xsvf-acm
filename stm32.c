/*
 * Copyright 2016 Tormod Volden
 * Portions from libopencm3 examples:
 * Copyright 2009 Uwe Hermann <uwe@hermann-uwe.de>
 * Copyright 2011 Piotr Esden-Tempski <piotr@esden.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/nvic.h>
#include "cdcacm.h"

void system_init(void) {
        /* Configure SysTick Timer for 1 ms ticks (delay functions) */
	rcc_clock_setup_in_hse_8mhz_out_72mhz();
	/* 72MHz / 8 => 9000000 counts per second */
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);
	/* 9000000/9000 = 1000 overflows per second - every 1ms one interrupt */
	systick_set_reload(8999);
	systick_interrupt_enable();
	systick_counter_enable();
}

/* Timer code */

volatile uint32_t TimingDelay ;

#if 0
void Delay( uint32_t nTime ) {
        TimingDelay = nTime ;
        while ( TimingDelay != 0);
}
#endif

void sys_tick_handler(void) {
        if ( TimingDelay != 0x00)
        TimingDelay --;
}
